Meteor.startup(function() {
    Router.route('/', {
        template: 'index'
    });


    Router.route('/index', {
        name: 'index',
        template: 'index'
    });

    Router.route('/home', {
        name: 'home',
        template: 'home'
    });

    Router.route('/new', {
        name: 'new',
        template: 'new_project'
    });

    Router.route('/project/:pid', {
        name: 'project',
        template: 'project',
        data: function () {
            pid = this.params.pid;
            return Projects.findOne({_id: pid});
        }
    });


    Router.route('/slydepay/:pay_token', {where: 'server'}).get(function () {
        this.response.writeHead(302, {
            'Location': "https://app.slydepay.com.gh/paylive/detailsnew.aspx?pay_token=" + this.params.pay_token
        });
        this.response.end();
    });

    Router.route('/logout', {
        name: 'logout'
    });

    Router.route('/discover', {
        name: 'discover',
        template: 'home'
    });

    Router.route('/transaction', {
        name: 'transaction',
        template: 'transaction'
    });
});