Meteor.startup(function () {
    //Configure Upload Server
    UploadServer.init({
        tmpDir: process.env.PWD + '/.uploads/tmp',
        uploadDir: process.env.PWD + '/.uploads/',
        checkCreateDirectories: true //create the directories for you
    });



    var wsdlurl = 'https://app.slydepay.com.gh/webservices/paymentservice.asmx?wsdl';
    var proxy=null;
    var uuid = Meteor.npmRequire('uuid');
    var Slydepay = Meteor.npmRequire('iwallet-node');
    var slydepay = new Slydepay(wsdlurl, "1.4", "merchantemail","merchantapikey", "C2B", 1, proxy);
    function buildOrder(item_id, project_name, amount) {
        order_item = slydepay.buildOrderItem(item_id, project_name, amount, 1, amount);
        return order_item;
    }

    function checkValidity(pay_token) {
        var validOrder = Payments.findOne({'order.pay_token':pay_token});
        return (validOrder.order.pay_token!=null || validOrder.order.order_id!=null);
    }

    function parseTransactionStatusCode(status_code) {
        status_code = parseInt(status_code);
        if (status_code == 0)
            return "success";
        else if (status_code == -2)
            return "cancelled";
        else if (status_code == -1)
            return "error";
        else
            return "unknown";
    }

    Meteor.methods({
        get_token: function(project_id, amount) {
            var args =  {
                "taxAmount":0,
                "total":amount,
                "orderItems":[],
                "shippingCost":0,
                "orderId": uuid.v1(),
                "comment2": "",
                "comment1": "",
                "subtotal":amount
            };
            var project = Projects.findOne({'_id':project_id});
            var order = buildOrder(project._id,project.project.basic_details.name,parseFloat(amount));
            JSON.stringify(args.orderItems);
            args.orderItems.push(order);
            console.log(args.orderItems);
            var token = Async.runSync(function(done) {
                slydepay.processPaymentOrder(args, function (err, results) {
                    done(null, results);
                    console.log(results);
                });
            });
            console.log(token.result.ProcessPaymentOrderResult);
            return [token.result.ProcessPaymentOrderResult, args.orderId];
        },

        process_payment: function(status_code, transaction_id, order_id, pay_token) {
            if (status_code==undefined || transaction_id==undefined || order_id==undefined || pay_token==undefined) {
                return "Transaction not processed"
            }

            var payment_status = parseTransactionStatusCode(status_code);
            var currentOrder = Payments.find({'order.order_id':order_id}).fetch();
            var payment_id = currentOrder[0]['_id'];
            if (transaction_id==null){
                Payments.update(payment_id,{'order.payment_status': 'FAILED'});
                return "Transaction failed: no transaction id"
            }

            if (!checkValidity(pay_token)) {
                return "No transaction corresponding to the received payment"
            }

            var order_result = Async.runSync(function(done) {
                slydepay.verifyMobilePayment({"orderId":order_id}, function (err, results) {
                    done(null, results);
                    if (err) {
                        console.log(err);
                    }
                });
            });


            if (order_result.result.verifyMobilePaymentResult.success) {
                Payments.update(payment_id,{$set: {'order.payment_status': payment_status, 'order.paid':'yes'}});
                var confirm_transaction = Async.runSync(function(done) {
                    slydepay.confirmTransaction({'payToken': pay_token, 'transactionId': transaction_id}, function(err, result) {
                        done(null, result);
                        if (result.ConfirmTransactionResult==1) {
                            Projects.update({'_id':currentOrder[0]['order']['project_id']}, {$inc: {"project.status.total_pledged": parseInt(currentOrder[0]['order']['amount'])}});
                            console.log("we're done!");
                            //return result;
                        }
                    });
                });
                return confirm_transaction;

            }

            else {
                Payments.update(payment_id,{'payment_status': payment_status});
                var cancel_transaction = Async.runSync(function(done) {
                    slydepay.cancelTransaction({'payToken': pay_token, 'transactionId': transaction_id}, function(err, result) {
                        done(null, result)
                    });
                });
                return cancel_transaction;
            }
        }
    });

});