Meteor.startup(function() {
    Uploader.uploadUrl = Meteor.absoluteUrl("upload"); // Cordova needs absolute URL
    Uploader.finished = function(index, file) {
        console.log(file.url);
        Projects.update(project_id, {$set: {"project.image_url": file.url}});
    }
});