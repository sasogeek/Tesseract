Template.register_modal.events({
    'submit form': function(event) {
        event.preventDefault();
        $('#registerButton').html('\
            <div class="preloader-wrapper big active">\
                <div class="spinner-layer spinner-blue">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
\
                <div class="spinner-layer spinner-red">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
\
                <div class="spinner-layer spinner-yellow">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
\
                <div class="spinner-layer spinner-green">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
            </div>');
        var username = event.target.registerUsername.value;
        var userEmail = event.target.registerEmail.value;
        var userPassword = event.target.registerPassword.value;

        Accounts.createUser({
            username: username,
            email: userEmail,
            password: userPassword

        }, function(error){
            if (error) {
                reason = error.reason;
                console.log(error);
                $('#registerErrorMessage').html(reason);
                $('#registerButton').html('<input class="btn btn-block blue white-text" type="submit" value="Register"/>');
            } else {
                $('#register').closeModal();
                Materialize.toast("Registration Complete @"+username, 4000)
                Materialize.toast("Proceed to login.", 4000)
                //$('#registrationComplete').text("Registration Complete @"+username+", proceed to login.");
                document.getElementById("registrationForm").reset();
                $('#registerButton').html('<input class="btn btn-block green white-text" type="submit" value="Register"/>');
                Meteor.logout();
            }
        });
    }
});