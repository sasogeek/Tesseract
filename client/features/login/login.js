Template.login_modal.events({
    'submit form': function(event) {
        event.preventDefault();
        $('#loginButton').html('\
            <div class="preloader-wrapper big active">\
                <div class="spinner-layer spinner-blue">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
\
                <div class="spinner-layer spinner-red">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
\
                <div class="spinner-layer spinner-yellow">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
\
                <div class="spinner-layer spinner-green">\
                    <div class="circle-clipper left">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="gap-patch">\
                        <div class="circle"></div>\
                    </div>\
                    <div class="circle-clipper right">\
                        <div class="circle"></div>\
                    </div>\
                </div>\
            </div>');
        var usernameOrEmail = event.target.loginUsernameOrEmail.value;
        var password = event.target.loginPassword.value;
        Meteor.loginWithPassword(usernameOrEmail, password, function(error){
            if (error) {
                reason = error.reason;
                console.log(error);
                $('#loginErrorMessage').html(reason);
                $('#loginButton').html('<input class="btn btn-block green white-text" type="submit" value="Login"/>');
            } else {
                $('#login').closeModal();
                Router.go('home');
            }
        });
    }
});