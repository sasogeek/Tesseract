Template.contribute_modal.events({
    'submit form': function (event) {
        event.preventDefault();
    $('.c_button').html('\
        <div class="preloader-wrapper big active">\
            <div class="spinner-layer spinner-blue">\
                <div class="circle-clipper left">\
                    <div class="circle"></div>\
                </div>\
                <div class="gap-patch">\
                    <div class="circle"></div>\
                </div>\
                <div class="circle-clipper right">\
                    <div class="circle"></div>\
                </div>\
            </div>\
\
            <div class="spinner-layer spinner-red">\
                <div class="circle-clipper left">\
                    <div class="circle"></div>\
                </div>\
                <div class="gap-patch">\
                    <div class="circle"></div>\
                </div>\
                <div class="circle-clipper right">\
                    <div class="circle"></div>\
                </div>\
            </div>\
\
            <div class="spinner-layer spinner-yellow">\
                <div class="circle-clipper left">\
                    <div class="circle"></div>\
                </div>\
                <div class="gap-patch">\
                    <div class="circle"></div>\
                </div>\
                <div class="circle-clipper right">\
                    <div class="circle"></div>\
                </div>\
            </div>\
\
            <div class="spinner-layer spinner-green">\
                <div class="circle-clipper left">\
                    <div class="circle"></div>\
                </div>\
                <div class="gap-patch">\
                    <div class="circle"></div>\
                </div>\
                <div class="circle-clipper right">\
                    <div class="circle"></div>\
                </div>\
            </div>\
        </div>');
        var p_id = $('#pid').val();
        var pledge_amount = event.target.pledge_amount.value;
        Meteor.call('get_token', p_id, pledge_amount, function (error, result) {
            if (error) {
                throw error;
            }
            if (result != undefined) {
                //Router.go('/slydepay/' + result);
                Payments.insert({
                    order: {
                        pay_token: result[0],
                        order_id: result[1],
                        project_id: p_id,
                        amount: pledge_amount,
                        payment_status: '',
                        created_by: Meteor.userId(),
                        created_on: new Date(),
                        paid: 'no'
                    }
                });
                var currentOrder = Payments.find({'order.order_id':result[1]}).fetch();
                console.log(currentOrder);
                Projects.update({'_id':currentOrder[0]['order']['project_id']}, {$push: {"project.backers": {"name": Meteor.user().username, "email": Meteor.user().emails[0].address, "amount_pledged": parseInt(currentOrder[0]['order']['amount']), "order_id":result[1]}}});
                console.log(result);
                try {
                    var ref = cordova.InAppBrowser.open('https://app.slydepay.com.gh/paylive/detailsnew.aspx?pay_token='+result[0],'_system');
                    //ref.addEventListener('loadstart', function(event) {
                    //    var urlSuccessPage = "http://10.8.23.84:3000/";
                    //    if (event.url.indexOf(urlSuccessPage) >= 0) {
                    //            setTimeout(function(){ ref.close(); },25000);
                    //    }
                    //});
                    $('.modal').closeModal();
                }
                catch (err) {
                    window.open('https://app.slydepay.com.gh/paylive/detailsnew.aspx?pay_token='+result[0], '_blank');
                    $('.modal').closeModal();
                }
                //catch(err) {
                //    window.open('https://app.slydepay.com.gh/paylive/detailsnew.aspx?pay_token='+result[0], '_blank');
                //    $('.modal').closeModal();
                //}
                $('.c_button').html('<input class="btn green white-text" type="submit" value="Contribute" />')
            }
        });
    }
});


Template.project.helpers({
    projects: function () {
        return Projects.find({}, {sort: {'project.created_on': -1}});
    },


    funding_goal: function (x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    total_pledged: function (x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    minimum_pledge: function (x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    funding_deadline: function (x) {
        return moment(x).format('LLL');
    },

    progress: function (id) {
        var project = Projects.findOne({_id: id});
        var progress = (project.project.status.total_pledged / project.project.basic_details.funding_goal) * 100;
        return progress
    },

    owner: function (id) {
        var project = Projects.findOne({_id: id});
        var owner = project.project.owner_details.full_name;
        return owner
    },

    owner_bio: function (id) {
        var project = Projects.findOne({_id: id});
        var bio = project.project.owner_details.biography;
        return bio
    },

    owner_username: function (id) {
        var project = Projects.findOne({_id: id});
        var username = project.project.owner_details.username;
        return username
    },

    owner_location: function (id) {
        var project = Projects.findOne({_id: id});
        var loc = project.project.owner_details.location;
        return loc
    },

    owner_email: function (id) {
        var project = Projects.findOne({_id: id});
        var email = project.project.owner_details.contact.email;
        return email
    },

    image: function(id) {
        return Images.findOne({'_id': id});
    }
});
