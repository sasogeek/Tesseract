Template.logout.events({
    'click #logout': function(){
        Meteor.logout();
        window.location.reload();
    }
});