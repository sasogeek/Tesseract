Template.transaction.onRendered(
    function() {
        setTimeout(function(){ window.close(); },15000);
    }
);

Template.transaction.helpers({
    results: function () {
        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        }


        var status_code = getURLParameter('status');
        var transaction_id = getURLParameter('transac_id');
        var order_id = getURLParameter('cust_ref');
        var pay_token = getURLParameter('pay_token');
        Meteor.call('process_payment', status_code, transaction_id, order_id, pay_token, function (error, result) {
            if (error) {
                throw error;
            }
            console.log(result);
            if (result.result.ConfirmTransactionResult==1) {
                Session.set("results", result.result.ConfirmTransactionResult);
                var project_id = Payments.findOne({'order.order_id':order_id}).order.project_id;
                var project = Projects.findOne({'_id':project_id});
                Session.set("project", project.project.basic_details.name);
            }
        });
        return Session.get("results");
    },

    project: function() {
        return Session.get("project");
    }
});