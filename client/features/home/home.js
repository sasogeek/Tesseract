Template.home.helpers({
    projects: function () {
        return Projects.find({},{sort: {'project.created_on':-1}});
    },

    funding_goal: function (x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    total_pledged: function (x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    funding_deadline: function(x) {
        return moment(x).fromNow()+', '+moment(x).format('LLL');
    }
});