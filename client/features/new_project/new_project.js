a = 1;
Template.new_project.events({
    'click #step1': function () {
        project_name = $('#project_name').val();
        Session.set("step_2", true);
        if (project_name!="") {
            project_id = Projects.insert({
                project: {
                    basic_details: {
                        name: project_name,
                        short_description: '',
                        category: '',
                        funding_deadline: '',
                        funding_goal: ''
                    },

                    rewards: [{
                        id: '',
                        project_id: '',
                        name: '',
                        description: '',
                        minimum_pledge_amount: ''
                    }],

                    story: {
                        long_description: '',
                        risks_and_challenges: ''
                    },

                    owner_details: {
                        id: Meteor.userId(),
                        username: Meteor.user().username,
                        full_name: '',
                        biography: '',
                        location: '',
                        websites: [],
                        contact: {
                            email: Meteor.user().emails[0].address,
                            phone: ''
                        }
                    },

                    team: [],

                    status: {
                        total_pledged: 0,
                        live: false,
                        state: 'incomplete'
                    },

                    backers: [{
                        name: '',
                        contact: {
                            email: '',
                            phone: ''
                        },
                        amount_pledged: ''
                    }],

                    created_on: new Date()

                }
            });

            $('#step_info').text("Upload an image for your project");
            $('#info_collector').html('Click Browse to choose an image, then click upload to upload that image<br/><br/>');
            $('#next_btn').html('<a class="waves-effect waves-light btn blue col" id="step_intm">Continue</a>');

        }
    },

    'click #step_intm' : function () {
        Session.set("step_2", false);
        $('#step_info').text("What is your project about? briefly...");
        $('#info_collector').html('<textarea id="project_description" class="materialize-textarea"  length="140" autofocus required></textarea> \
                                   <label for="project_description">Project description</label> \
                                   ');
        $('#project_description').characterCounter();
        $('#next_btn').html('<a class="waves-effect waves-light btn blue col" id="step2">Continue</a>');
        $('#project_description').focus();
    },

    'click #step2': function () {
        project_description = $('#project_description').val();
        if (project_description!="") {
            Projects.update(project_id, {$set: {"project.basic_details.short_description": project_description}});
            Projects.update(project_id, {$set: {"project.status.state": 'incomplete'}});
            $('#step_info').text("Why do you want to pursue this project?");
            $('#info_collector').html('<textarea id="why" class="materialize-textarea"  length="1000" autofocus required></textarea> \
                                       <label for="why">Why this project?</label>');
            $('#why').characterCounter();
            $('#next_btn').html('<a class="waves-effect waves-light btn blue col" id="step3">Continue</a>');
            $('#why').focus();
        }
    },

    'click #step3': function () {
        why = $('#why').val();
        if (why!="") {
            Projects.update(project_id, {$set: {"project.story.long_description": why}});
            Projects.update(project_id, {$set: {"project.status.state": 'incomplete'}});
            $('#step_info').text("Who will you be working with on this project? Leave blank if you're working alone...");
            $('#info_collector').html('<input id="team_member" type="text" class="validate" autofocus>\
                                       <label for="team_member">Team Member name</label>\
                                       ');
            $('#next_btn').html('<a class="waves-effect waves-light btn green col" id="add_another">Add another person</a>\
                                 <a class="waves-effect waves-light btn blue col" id="step4">Continue</a>');
            $('#team_member').focus();
        }
    },

    'click #add_another': function () {
        team_member = $('#team_member').val();
        if (team_member!="") {
            Projects.update(project_id, {$push: {"project.team": team_member}});
            $('#step_info').text("Who will you be working with on this project? Leave blank if you're working alone...");
            $('#info_collector').html('<input id="team_member" type="text" class="validate" autofocus>\
                                       <label for="team_member">Team Member name</label>\
                                       ');
            $('#misc').prepend('<br/><div class="chip">'+team_member+'</div>');
            $('#next_btn').html('<a class="waves-effect waves-light btn green col" id="add_another">Add another person</a>\
                                 <a class="waves-effect waves-light btn blue col" id="step4">Continue</a>');
            $('#team_member').focus();
        }
    },

    'click #step4': function () {
        team_member = $('#team_member').val();
        Projects.update(project_id, {$push: {"project.team": team_member}});
        Projects.update(project_id, {$set: {"project.status.state": 'incomplete'}});
        $('#step_info').text("Awesome! Now kindly tell us about your challenges and how you're qualified to overcome them...");
        $('#info_collector').html('<textarea id="challenges" class="materialize-textarea"  length="1000" required autofocus></textarea> \
                                   <label for="challenges">What are your challenges</label>\
                                   ');
        $('#misc').html('');
        $('#next_btn').html('<a class="waves-effect waves-light btn blue col" id="step5">Continue</a>');
        $('#challenges').focus();
    },

    'click #step5': function () {
        challenges = $('#challenges').val();
        if (challenges!="") {
            Projects.update(project_id, {$set: {"project.story.risks_and_challenges": challenges}});
            Projects.update(project_id, {$set: {"project.status.state": 'incomplete'}});
            $('#step_info').text("And how much funds are you expecting to raise?");
            $('#info_collector').html('<div class="row">\
                                            <div class="input-field col s6">\
                                                  <input id="funding_goal" type="number" class="validate" required autofocus>\
                                                  <label for="funding_goal">Funding goal (GHC)</label>\
                                            </div>\
                                            <div class="input-field col s6">\
                                                  <input id="funding_deadline" type="date" class="datepicker" required>\
                                                  <label for="funding_deadline">Deadline</label>\
                                            </div>\
                                       </div>\
                                       ');
            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15, // Creates a dropdown of 15 years to control year
                format: 'yyyy-mm-dd'
              });
            $('#next_btn').html('<a class="waves-effect waves-light btn blue col" id="step6">Continue</a>');
            $('#funding_goal').focus();
        }
    },

    'click #step6': function () {
        funding_deadline = $('#funding_deadline').val();
        funding_goal = $('#funding_goal').val();
        if (funding_deadline!="" && funding_goal!="") {
            Projects.update(project_id, {$set: {"project.basic_details.funding_deadline": funding_deadline, "project.basic_details.funding_goal": funding_goal}});
            Projects.update(project_id, {$set: {"project.status.state": 'incomplete'}});
            $('#step_info').text("Fantastic! Now that you're done, please setup your rewards for those who will support you, and your project will be ready for review.");
            $('#info_collector').html('<div class="row">\
                                            <div class="input-field col s12">\
                                                  <input id="reward_name" type="text" class="validate" required autofocus>\
                                                  <label for="reward_name">Reward name, eg. Starter Pack</label>\
                                            </div>\
                                            <div class="input-field col s12">\
                                                  <input id="reward_description" type="text" class="validate" required>\
                                                  <label for="reward_description">Reward description, eg. T-shirt</label>\
                                            </div>\
                                            <div class="input-field col s12">\
                                                  <input id="pledge_amount" type="number" class="validate" required>\
                                                  <label for="pledge_amount">Minimum pledge amount (GHC)</label>\
                                            </div>\
                                       </div>\
                                       ');
            $('#next_btn').html('<a class="waves-effect waves-light btn green col" id="add_reward"><i class="material-icons right">add</i>Add another reward</a>' +
                                '<a class="waves-effect waves-light btn blue col" id="step7">Continue</a>');
            $('#reward_name').focus();
        }
    },

    'click #add_reward': function () {
        a = a+1;
        reward_name = $('#reward_name').val();
        reward_description = $('#reward_description').val();
        pledge_amount = $('#pledge_amount').val();
        if (reward_name != "" && reward_description != "" && pledge_amount != "") {
            Projects.update(project_id, {
                $push: {
                    "project.rewards": {
                        "id": a,
                        "project_id": project_id,
                        "name": reward_name,
                        "description": reward_description,
                        "minimum_pledge_amount": pledge_amount
                    }
                }
            });
            Projects.update(project_id, {$set: {"project.status.state": 'review'}});
            $('#step_info').text("Fantastic! Now that you're done, please setup your rewards for those who will support you, and your project will be ready for review.");
            $('#info_collector').html('<div class="row">\
                                            <div class="input-field col s12">\
                                                  <input id="reward_name" type="text" class="validate" required autofocus>\
                                                  <label for="reward_name">Reward name, eg. Starter Pack</label>\
                                            </div>\
                                            <div class="input-field col s12">\
                                                  <input id="reward_description" type="text" class="validate" required>\
                                                  <label for="reward_description">Reward description, eg. T-shirt</label>\
                                            </div>\
                                            <div class="input-field col s12">\
                                                  <input id="pledge_amount" type="number" class="validate" required>\
                                                  <label for="pledge_amount">Minimum pledge amount (GHC)</label>\
                                            </div>\
                                       </div>\
                                       ');
            $('#misc').prepend('<br/><div class="row">\
                                        <div class="col s12 m6">\
                                          <div class="card blue-grey darken-1">\
                                            <div class="card-content white-text">\
                                              <span class="card-title">'+reward_name+'</span>\
                                              <p>'+reward_description+'</p>\
                                              <p><strong>Minimum Pledge:</strong> GHC '+pledge_amount+'</p>\
                                            </div>\
                                          </div>\
                                        </div>\
                                      </div>');
            $('#next_btn').html('<a class="waves-effect waves-light btn green col" id="add_reward">Add another reward</a>' +
                                '<a class="waves-effect waves-light btn blue col offset-m1" id="step7">Continue</a>');
            $('#reward_name').focus();
        }
    },

    'click #step7': function () {
        a=a+1;
        reward_name = $('#reward_name').val();
        reward_description = $('#reward_description').val();
        pledge_amount = $('#pledge_amount').val();
        if ((reward_name!="" && reward_description!="" && pledge_amount!="") || Projects.findOne(project_id, {$where:"project.rewards.length>1"})) {
            Projects.update(project_id, {$push: {"project.rewards": {"id": a, "project_id": project_id, "name": reward_name, "description": reward_description, "minimum_pledge_amount": pledge_amount}}});
            Projects.update(project_id, {$set: {"project.status.state": 'review'}});
            $('#step_info').text("Your project has been submitted for review! But, please fill in your details, THIS IS A PRIMARY REQUIREMENT for your project to even be considered for the review process.");
            $('#info_collector').html('<div class="row">\
                                            <div class="input-field col s12">\
                                                  <input id="full_name" type="text" class="validate" required autofocus>\
                                                  <label for="full_name">Full name</label>\
                                            </div>\
                                            <div class="input-field col s12">\
                                                  <input id="phone" type="tel" class="validate" required>\
                                                  <label for="phone">Phone Number</label>\
                                            </div>\
                                            <div class="input-field col s12">\
                                                  <textarea id="bio" class="materialize-textarea"  length="1000" required></textarea>\
                                                  <label for="bio">Biography (A little about yourself)</label>\
                                            </div>\
                                            <div class="input-field col s12">\
                                                  <input id="location" type="text" class="validate" required>\
                                                  <label for="location">Location (eg, Accra, Ghana)</label>\
                                            </div>\
                                       </div>\
                                       ');
            $('#misc').html('');
            $('#next_btn').html('<a class="waves-effect waves-light btn green col" id="step8">Save</a>');
            $('#full_name').focus();
        }
        a=0;
    },

    'click #step8': function () {
        full_name = $('#full_name').val();
        phone = $('#phone').val();
        bio = $('#bio').val();
        loc = $('#location').val();
        if (full_name!="" && phone!="" && bio!="" && location!="") {
            Projects.update(project_id, {$set: {"project.owner_details.full_name": full_name, "project.owner_details.contact.phone": phone, "project.owner_details.biography": bio, "project.owner_details.location": loc}});
            Projects.update(project_id, {$set: {"project.status.state": 'review'}});
            $('#step_info').text("Your project is setup for review. You'll hear from us soon.");
            $('#info_collector').html('<a href="'+window.location.protocol + "//" + window.location.host+'/home">Discover other great projects!</a>');
            $('#misc').prepend('');
            $('#next_btn').html('');
        }
        a=0;
    },

    'submit form': function(event) {
        event.preventDefault();
    }
});

Template.new_project.helpers({
    step_2: function() {
        return Session.get("step_2")
    }
});